<?php

/**
* Authentication crontroller
*/
class AuthController extends Controller
{

	public function getLogin(){
		return View::make('login');
	}

	public function getLogout() {
	    Auth::logout();
	    sleep(1);
	    return Redirect::to('login')->withErrors(array('Your are now logged out!'));
	}

	public function postLogin(){
		$rules = array(
				'username'	=> 'required',
				'password'	=> 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Response::json(['success' => false,'errors' => ['login' => ['Please check your inputs']], 400]);
		}
		$save = Input::get('saveUsr');
		$auth = Auth::attempt(array(
				'username'		=> Input::get('username'),
				'password'	=> Input::get('password')
			), $save); // the false is for the remember me

		if (!$auth) {
			return Response::json(['success' => false,'errors' => ['login' => ['Invalid credentials']], 400]);
		} else {
			return Response::json(['success' => true], 200);
		}

	}
}
