<?php

class BlogsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$blogs = Auth::user()->blogs;
		$user = Auth::user();
		
		return Response::json($blogs);
//		return View::make('home', array(
//				'blogs'	=> $blogs,
//				'user' => $user
//			));
	}

	public function indexBy($id)
	{
		$user = User::find($id);
		$blogs = $user->blogs;
		//TODO: Needs to return the json of the return
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// displays the new post form
		return View::make('new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// saves a new blog in the database
		$rules = array(
				'title'	=> 'required|min:3|max:255',
				'content' => 'required'
			);

		$validator = Validator::make(Input::all(), $rules);
		// $userId = Auth::user()->id;

		if ($validator->fails()) {
			// return Redirect::route('new')->withErrors($validator);

			$this->res_err_data['success'] = false;
			$this->res_err_data['error'] = $validator->messages();
        	return Response::json($this->res_err_data, 200);
			//return Response::json($validator->messages, 200); 
		}

		$blog = new Blog;
		$blog->title = Input::get('title');
		$blog->content = Input::get('content');
		// TODO Add a method to add the image link in the database
		/*if (Input::hasFile('filesToUpload')){			
			$destinationPath = public_path().'uploads'; // upload path
	      	$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
	      	$fileName = rand(11111,99999).'.'.$extension; // renameing image
	      	Input::file('image')->move($destinationPath, $fileName); // uploading file to given path

	      	$blog->image = $destinationPath.'/'.$fileName;
		}*/

	    $destinationPath = public_path().'/uploads';

	    $file = Input::file('image');
		if (Input::hasFile('image'))
		{
		    $extension = $file->getClientOriginalExtension();
		    $filename = str_random(12).".{$extension}";
		    $upload_success = $file->move($destinationPath, $filename);
		    $blog->image = 'uploads/'.$filename;
		}

		$blog->user_id = Auth::user()->id;


		if($blog->save()){
			return Response::json(['success' => true], 200); 
		} else {
			return Response::json(['success' => false], 200); 
		}

		// return Redirect::route('home');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// displays specific post
		$blog = Blog::find($id);

//			if ($blog->user_id == Auth::user()->id) {	
//			return View::make('single', array(
//					'blog' => $blog
//				));
//		}
		return View::make('single', array(
					'blog' => $blog
				));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// shows the edit form
		$blog = Blog::find($id);
		return View::make('edit', array(
				'blog'	=> $blog
			));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// saves the changes made to the post
		$rules = array(
				'title'	=> 'required|min:3|max:255',
				'content' => 'required'
			);

		$validator = Validator::make(Input::all(), $rules);
		// $userId = Auth::user()->id;

		if ($validator->fails()) {
			return Redirect::route('new')->withErrors($validator);
		}

		$blog = Blog::find($id);
		$blog->title = Input::get('title');
		$blog->content = Input::get('content');
		// TODO Add a method to add the image link in the database
		
		

		$blog->save();

		return Redirect::to('blog/'.$id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($blog)
	{
		// deletes a post
		if ($blog->user_id == Auth::user()->id) {
			$blog->delete();

		}		
		return Redirect::route('home');
	}


}
