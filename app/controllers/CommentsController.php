<?php

class CommentsController extends \BaseController {


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$comment =  new Comment;
		$userId = Auth::user()->id;
		
		$comment->user_id = $userId;
		
		$comment->blog_id = Input::get('blogID');
		
		$comment->content = Input::get('content');
		// TODO Add a method to add the image link in the database

		if($comment->save()){
			$comment->ago = $comment->created_at->diffForHumans();
			$comment->commentor = User::find($comment->user_id)->name;
			return Response::json(['success' => true, 'details' => $comment], 200);
		} else {
			return Response::json(['success' => false], 200);
		}
	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
