<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates the users table
		Schema::create('users', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('username');
			$table->string('password');
			$table->boolean('active');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Deletes the users table
		Schema::drop('users');
	}

}
