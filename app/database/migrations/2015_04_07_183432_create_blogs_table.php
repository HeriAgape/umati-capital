<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates the blog table
		Schema::create('blogs', function(Blueprint $table){
			$table->increments('id');
			$table->integer('user_id');
			$table->string('title');
			$table->text('content');
			$table->string('image');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Deletes the blogs table
		Schema::drop('blogs');
	}

}
