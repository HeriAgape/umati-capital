<?php

/**
* Populates the users table
*/
class CommentsTableSeeder extends Seeder
{

	public function run(){
		DB::table('comments')->delete();

		$comments = array(
				array(
						'user_id' 		=> 	1,
						'blog_id' 		=> 	1,
						'content' 	=>	'Very well done system!'
					),
				array(
						'user_id' 		=> 	1,
						'blog_id' 		=> 	2,
						'content' 	=>	'Good Job'
					),
			);

		DB::table('comments')->insert($comments);
	}
}