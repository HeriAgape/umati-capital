<?php

/**
* Populates the users table
*/
class UsersTableSeeder extends Seeder
{

	public function run(){
		DB::table('users')->delete();

		$users = array(
				array(
						'name' 		=> 	'John Terry',
						'username' 		=> 	'john',
						'password' 	=>	Hash::make('john'),
						'email'		=>	'example@example.com'
					)
			);

		DB::table('users')->insert($users);
	}
}