<?php 
/**
* 
*/
class Blog extends Eloquent
{
	protected $table = 'blogs';

	protected $appends = array('exert', 'commentsj', 'author', 'createdtime');


	public function getExertAttribute(){

	return $this->words($this->content, 24);

	}

	public function getCreatedtimeAttribute(){

		return $this->created_at->diffForHumans();

	}

	public function getCommentsjAttribute(){

		return Comment::where('blog_id', '=', $this->id)->get();

	}

	public function getAuthorAttribute(){

		return $this->user->name;

	}

	
	public function user(){
		return $this->belongsto('User');
	}

	public function comments(){
		return $this->hasMany('Comment', 'blog_id');
	}


		// just the excerpt of a text (borrowed form the net)
	public function words($text, $number_of_words) {
	   // Where excerpts are concerned, HTML tends to behave
	   // like the proverbial ogre in the china shop, so best to strip that
	   $text = strip_tags($text);

	   // \w[\w'-]* allows for any word character (a-zA-Z0-9_) and also contractions
	   // and hyphenated words like 'range-finder' or "it's"
	   // the /s flags means that . matches \n, so this can match multiple lines
	   $text = preg_replace("/^\W*((\w[\w'-]*\b\W*){1,$number_of_words}).*/ms", '\\1', $text);

	   // strip out newline characters from our excerpt
	   return str_replace("\n", "", $text);
	}
}