<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function isActive(){
		return $this->active;
	}

	public function activate(){
		$this->activate = 1;
		return $this->save() ? true : false;
	}
	public function deactivate(){
		$this->activate = 0;
		return $this->save() ? true : false;
	}

	public function blogs(){
		return $this->hasMany('Blog', 'user_id');
	}
	
	public function comments(){
		return $this->hasMany('Comment', 'user_id');
	}


}
