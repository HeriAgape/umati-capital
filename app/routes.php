<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::bind('blog', function($value, $route){
	return Blog::where('id', $value)->first();
});

Route::get('/homeAjax', array(
		'as' 	=> 	'home',
		'uses' 	=> 	'BlogsController@getIndex'
	))->before('auth');

Route::get('/ajax', array(
		'as' 	=> 	'homepage',
		'uses' 	=> 	'HomeController@homepage'
	));
	
// ==================VIEWS OF THE HOME=============
Route::get('/', function(){
	
	return View::make('homepage');
});

Route::get('/home', function(){
	$user = Auth::user();
	return View::make('home', array('user' => $user));
});

// =================================================

// =============BLOGS SPECIFIC TO USER=============
Route::get('/profile/{username}', array(
		'as' 	=> 	'profile',
		'uses' 	=> 	'UsersController@index'
	));

// ===============DELETE A POST=====================
Route::get('/delete/{blog}', array(
		'as' 	=> 	'delete',
		'uses' 	=> 	'BlogsController@destroy'
	))->before('auth');
// ===================================================

// ==============DISPLAY SINGLE POST==================
Route::get('/blog/{id}', array(
		'as' 	=> 	'single',
		'uses' 	=> 	'BlogsController@show'
	));
// ===================================================


// ==============NEW POST ROUTES=====================
Route::get('/new', array(
		'as' 	=> 	'new',
		'uses' 	=> 	'BlogsController@create'
	))->before('auth');

Route::post('/saveBlog', array(
		'as' 	=> 	'saveBlog',
		'uses' 	=> 	'BlogsController@store'
	))->before('csrf');
// ===================================================


// ==================LOGIN ROUTES=====================
Route::get('/login', array(
		'as'	=> 'login',
		'uses'	=> 'AuthController@getLogin'
	))->before('guest');

// Ajax login
Route::post('postLogin', array(
		'as'	=> 'postLogin',
		'uses'	=> 'AuthController@postLogin'
	))->before('csrf');

// ===================================================

// ===============EDIT A POST=====================
Route::get('/edit/{id}', array(
		'as' 	=> 	'edit',
		'uses' 	=> 	'BlogsController@edit'
	))->before('auth');

Route::post('/edit/{id}', array(
		'uses' 	=> 	'BlogsController@update'
	))->before('csrf');
// ===================================================

// ==================LOGOUT ROUTES=====================
Route::get('/logout', array(
		'as'	=> 'logout',
		'uses'	=> 'AuthController@getLogout'
	));
// ===================================================

// ============COMMENTS=========================
Route::post('addcomment', array(
		'as'	=> 'addcomment',
		'uses'	=> 'CommentsController@store'
	))->before('csrf');