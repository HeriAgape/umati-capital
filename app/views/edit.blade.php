@extends('layouts.main')

@section('menu')
<ul class="left">
        <li><a href="{{ route('home') }}"><i class="mdi-navigation-arrow-back"></i></a></li>
        
</ul> 
<ul class="right">
        <li><a href="{{ '/delete/'.$blog->id }}"><i class="mdi-action-delete"></i></a></li>
</ul> 
@stop

@section('content')

	@foreach ($errors->all() as $error)
		<p class="error">{{ $error }}</p>
	@endforeach
	<div class="row">

<div class="row">
        {{ Form::model($blog, array('class'=>'col s12')) }}
          
            
              <h4>
    <i class="mdi-action-bookmark prefix"></i> Edit Article</h4>
              <div class="row">
		<div class="input-field col s12">
			<input id="post-title" type="text" name="title" class="validate" value="{{ $blog->title }}" />
			<label for="post-title">Title</label>
		</div>
		<div class="input-field col s12">
          <textarea name="content" id="icon_prefix2" class="materialize-textarea" rows="19">{{ $blog->content }}</textarea>
          <label for="icon_prefix2">Content</label>
        </div>

<div class="input-field col s12">
		<div class="file-field input-field">
      <input class="file-path validate" type="text"/>
      <div class="btn">
        <input name="image" type="file" />
        <i class="mdi-file-attachment"></i>
      </div>
    </div>
</div>
	</div>
              <button style="width: 100%" type="submit" class="btn btn-large waves-effect waves-light right" name="action">Update 
		<i class="mdi-content-send right"></i></button>
   
  
        {{ Form::close() }}
      </div>
</div>
@stop