@extends('layouts.main')
@section('menu') 
<ul class="right">
        <li style="padding:0 10px;">{{ $user->name }}</li>
        <li><a class="dropdown-button" href="#!" data-activates="userMenu"><i class="mdi-action-account-circle" style="padding:0 10px;"></i></a></li>
</ul> 

@stop

@section('content')

<!-- Dropdown Structure -->
<ul id="userMenu" class="dropdown-content">
  <li><a href="#!">Edit</a></li>
  <li><a href="#!">Other</a></li>
  <li class="divider"></li>
  <li><a href="/logout">Logout</a></li>
</ul>

	{{-- Preloaders --}}
	{{-- <div class="progress">
      <div class="indeterminate"></div>
  </div> --}}
  <h2>{{ $user->name }}'s Articles</h2>

	<div class="row">
	    <div class="col s12 content">
	      
	    </div>
	  </div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	  		<a class="btn-floating btn-large blue-grey waves-effect waves-orange" href="{{ URL::route('new') }}">
	    		<i class="large mdi-content-add"></i>
	  		</a>
	</div>
@stop

@section('script')
<script>
$('#loader').show();


$(document).ready(function(){
  url = '{{ URL::route('home') }}';
  $.getJSON( url, function( data ) {
  var items = [];
  $.each( data, function( key, val ) {
    var structure = [
    '<div class="card">',
        '<div class="card-image waves-effect waves-block waves-light">',
          '<img class="activator" src="'+val.image+'">',
        '</div>',
        '<div class="card-content">',
      '<div>',
            '<i class="mdi-action-assessment circle green"></i>',
          '</div>',
          '<span class="card-title activator grey-text text-darken-4">'+val.title+' <i class="mdi-navigation-more-vert right"></i></span>',
          '<p><a href="/blog/'+val.id+'">Read More</a></p>',
        '</div>',
        '<div class="card-reveal">',
          '<span class="card-title grey-text text-darken-4">'+val.title+' <i class="mdi-navigation-close right"></i></span>',
          '<p>'+val.exert+'</p>',
        '</div>',
      '</div>'
];
    items.push(structure.join(''));
  });
 $('#loader').hide();
  $( "<ul/>", {
    "id": "postList",
    html: items.join( "" )
  }).appendTo( ".content" );
});
  
})

</script>
@stop