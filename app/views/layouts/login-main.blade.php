<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <title>Our ToDO Application</title>
 {{--  <script type="text/javascript" src="{{ URL::asset('bower_components/webcomponentsjs/webcomponents.min.js') }}"> </script> --}}
   <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="{{ URL::asset('includes/materialize/css/materialize.min.css') }}">
 {{--  <link rel="import" href="{{ URL::asset('elements/card-image.html') }}"> --}}
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
  <style>
    /* body {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
      }
      main {
    flex: 1 0 auto;
      } */
  footer.page-footer {
 
  background-color: transparent;
}
footer.page-footer .footer-copyright {
  color: #000;
  background-color: transparent;
}
  </style>
</head>
<body>

<div class="container">

<div class="row">
<div class="col s12 m6 body">

	@yield('content')

</div>
</div>
</div>
<footer class="page-footer">
          
          <div class="footer-copyright">
            <div class="container">
              © 2015 Heri Agape
            <a class="grey-text right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
<script type="text/javascript" src="{{ URL::asset('includes/jquery-2.1.1.min.js') }}"></script>
   <!-- Compiled and minified JavaScript -->
  <script type="text/javascript" src="{{ URL::asset('includes/materialize/js/materialize.min.js') }}"></script>
  <script>
  $(document).ready(function(){
        @if ($errors->all())  
          @foreach ($errors->all() as $error)
            Materialize.toast('{{{ $error }}}', 4000, 'rounded');
          @endforeach    
        @endif
  });
  </script>

  @yield('script')

</body>
</html>
