<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <title>Our ToDO Application</title>
 {{--  <script type="text/javascript" src="{{ URL::asset('bower_components/webcomponentsjs/webcomponents.min.js') }}"> </script> --}}
   <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="{{ URL::asset('includes/materialize/css/materialize.min.css') }}">
 {{--  <link rel="import" href="{{ URL::asset('elements/card-image.html') }}"> --}}
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
</head>
<body>
<div class="navbar-fixed">
<nav class="">
    <div class="nav-wrapper white">
      <div class="">
        {{-- @section('sidebar') --}}
        @yield('menu')
      {{-- <ul class="right">
        <li><a href="sass.html"><i class="mdi-action-search"></i></a></li>
        <li><a href="components.html"><i class="mdi-action-view-module"></i></a></li>
        <li><a href="javascript.html"><i class="mdi-navigation-refresh"></i></a></li>
        <li><a href="mobile.html"><i class="mdi-navigation-more-vert"></i></a></li>
      </ul> --}}
      {{-- @stop --}}
    </div>
      </div>
  </nav>
  </div>
<div class="container">

<div class="row">
<div class="col s12 m6 body">

	@yield('content')

</div>
</div>
</div>
<script type="text/javascript" src="{{ URL::asset('includes/jquery-2.1.1.min.js') }}"></script>
   <!-- Compiled and minified JavaScript -->
  <script type="text/javascript" src="{{ URL::asset('includes/materialize/js/materialize.min.js') }}"></script>

  <footer class="page-footer grey darken-2">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2015 Heri Agape
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>

         @yield('script')
</body>
</html>
