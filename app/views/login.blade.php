@extends('layouts.login-main')

@section('content')
<div class="row">
	    <div style="margin-top: 30px;" class="col s12">
		
		<div class="login-icon">
			<i class="mdi-action-account-circle"></i>
		</div>
<!-- 		@if ($errors->all())
<div class="card-panel  orange lighten-1">
	@foreach ($errors->all() as $error)
<p class="error">{{ $error }}</p>
	@endforeach
</div>

@endif -->

	{{ Form::open(array('autocomplete' => 'off',
							'url' => 'postLogin',
							'id' => 'loginFrm',
							'method' => 'post'		)) }}

		<p class="input-field">
          <input id="icon_prefix" type="text" class="validate" name="username">
          <label for="icon_prefix">Username</label>
        </p>
		
		<p class="input-field">
          <input id="icon_pass" type="password" class="validate" name="password">
          <label for="icon_pass">Password</label>
        </p>

		<p class="input-field">
      <input type="checkbox" class="filled-in" name="saveUsr" id="saveUsr" />
      <label for="saveUsr">Stay logged in</label>
    </p>
    <br/>
		<p>
			<input style="width: 100%;" class="btn-large blue-grey lighten-1" type="submit" value="Sign in" />
		</p>
	{{ Form::close() }}
</div>
</div>
@stop

@section('script')
<script>
	$("#loginFrm").submit(function() {

     var FrmData = $("#loginFrm").serializeArray();
     var url      = $('#loginFrm').attr('action');

    $.ajax({
           type: "POST",
           url: url,
           data: FrmData, // serializes the form's elements.
           success: function(data)
           {

           	if(!data.success){
               $.each( data.errors, function( key, val ) {

               		Materialize.toast(val, 4000);
				    
				  });
           	} else {
           		window.location.replace("/home");
           	}

           }
         });

    return false; // avoid to execute the actual submit of the form.
});
</script>
@stop