@extends('layouts.main')

@section('menu')
<ul class="left">
        <li><a href="{{ route('home') }}"><i class="mdi-navigation-arrow-back"></i></a></li>
        
</ul> 
@stop

@section('content')

	@foreach ($errors->all() as $error)
		<p class="error">{{ $error }}</p>
	@endforeach
	<div class="row">

<div class="row">

         {{ Form::open(array('class'=>'col s12', 'enctype'=>'multipart/form-data',
                              'url' => 'saveBlog',
                              'id' => 'newFrm',
                              'files' => true,
                              'method' => 'post')) }}
    
<div class="progress">
      <div class="indeterminate"></div>
  </div>
                <h4>
    <i class="mdi-action-description prefix"></i> Create Article</h4>
              <div class="row">
    <div class="input-field col s12">


      <input id="post-title" type="text" name="title" class="validate" />
      <label for="post-title">Title</label>
    </div>
    <div class="input-field col s12">
          <textarea name="content" id="icon_prefix2" class="materialize-textarea" rows="19"></textarea>
          <label for="icon_prefix2">Content</label>
        </div>

<div class="input-field col s12">
    <div class="file-field input-field">
      <input class="file-path validate" type="text"/>
      <div class="btn">
        <input id="imageContent" name="image" type="file" />
        <i class="mdi-file-attachment"></i>
      </div>
    </div>
</div>
  </div>
              <button style="width: 100%" type="submit" class="btn btn-large waves-effect waves-light right" name="action">Create 
    <i class="mdi-content-send right"></i></button>
   
  
     
        {{ Form::close() }}
      </div>
</div>
@stop

@section('script')
<script>
$('.progress').hide('fast');

 function disableFRM() {
    var limit = document.forms[0].elements.length;
    for (i=0;i<limit;i++) {
      document.forms[0].elements[i].disabled = true;
    }
  }

  function enableFRM() {
    var limit = document.forms[0].elements.length;
    for (i=0;i<limit;i++) {
      document.forms[0].elements[i].disabled = false;
    }
  }

  $("#newFrm").submit(function() {

  $('.progress').show('fast');

      var FrmData = new FormData($('#newFrm')[0]);
     // var FrmData = $("#newFrm").serializeArray();
     var url      = $('#newFrm').attr('action');

     // FrmData.append("image", $('#imageContent').prop('files'));

     disableFRM();


    $.ajax({
           type: "POST",
           url: url,
           data: FrmData, // serializes the form's elements.
           processData: false, // very important portion of the file upload
            contentType: false, // very important portion of the file upload
           success: function(data)
           {

            if(!data.success){
              //alert('data');
               $.each( data.error, function( key, val ) {

                  Materialize.toast(val, 4000);
                  // alert('data');
                enableFRM();
                });
            } else {

              Materialize.toast('Article successfully added!', 4000);
              enableFRM();

            }

           }
         });

    $('.progress').hide('fast');
    return false; // avoid to execute the actual submit of the form.
});
</script>

@stop