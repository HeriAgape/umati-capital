@extends('layouts.main')

@section('menu')
<ul class="left">
        <li><a href="{{ route('home') }}"><i class="mdi-navigation-arrow-back"></i></a></li>
        
</ul> 
<ul class="right">
		<li><a href="#!"><i class="mdi-social-share"></i></a></li>
        <li><a href="{{ '/edit/'.$blog->id }}"><i class="mdi-editor-mode-edit"></i></a></li>
        <li><a href="{{ '/delete/'.$blog->id }}"><i class="mdi-action-delete"></i></a></li>
        
</ul> 
@stop

@section('content')

	{{-- Preloaders --}}
{{-- 	<div class="progress">
      <div class="indeterminate"></div>
  </div> --}}

	<div class="row">
	    <div class="col s12">
	      <h1>{{ $blog->title }}</h1>
	      <img class="materialboxed" src="{{ '/'.$blog->image }}" alt="{{ $blog->title }}">
	      <p class="flow-text">{{ $blog->content }}</p>
	    </div>
	  </div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	  		<a class="btn-floating btn-large blue-grey waves-effect waves-orange" href="{{ URL::route('new') }}">
	    		<i class="large mdi-content-add"></i>
	  		</a>
	</div>
  
  <div class="comments">

    <br/>
    <h4>Comments</h4>
    <div class="col s12">
      @foreach($blog->commentsj as $comment)
        <div class="card-panel grey lighten-5 z-depth-1">
          <div class="row valign-wrapper">
            <div class="col s2">
              <img src="http://lorempixel.com/200/200/" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
            </div>
            <div class="col s10">
              <h5>{{ User::find($comment->user_id)->name }}</h5>
              <span class="black-text">
                {{ $comment->content }}
              </span> <br>
              <small class="right"><i>{{ $comment->created_at->diffForHumans() }}</i></small>
            </div>
          </div>
        </div>
        @endforeach
      </div>
 
  </div>
  
  
  @if (!Auth::guest())
<div class="add-comment">
  <br/> <br/>
    <p>Add Comment</p>
    <div class="row">
    {{ Form::open(array('class'=>'col s12', 'enctype'=>'multipart/form-data',
                              'url' => 'addcomment',
                              'id' => 'commentFrm',
                              'method' => 'post')) }}
      <div class="row">
        <div class="input-field col s12">
          <textarea id="content" name="content" class="materialize-textarea"></textarea>
          <label for="textarea1">Textarea</label>
        </div>
      </div>
      <input name="blogID" type="text" value="{{ $blog->id }}" style=" display:none;">
      <button class="btn waves-effect waves-light" type="submit">Add Comment
    <i class="mdi-content-send right"></i>
  </button>
    {{ Form::close() }}
  </div>
  </div>
  @endif
  
@stop

@section('script')
<script>

 function disableFRM() {
    var limit = document.forms[0].elements.length;
    for (i=0;i<limit;i++) {
      document.forms[0].elements[i].disabled = true;
    }
  }

  function enableFRM() {
    var limit = document.forms[0].elements.length;
    for (i=0;i<limit;i++) {
      document.forms[0].elements[i].disabled = false;
    }
  }

  $("#commentFrm").submit(function() {

      var FrmData = $('#commentFrm').serializeArray();
     // var FrmData = $("#newFrm").serializeArray();
     var url      = $('#commentFrm').attr('action');

     // FrmData.append("image", $('#imageContent').prop('files'));

     disableFRM();


    $.ajax({
           type: "POST",
           url: url,
           data: FrmData, // serializes the form's elements.
           success: function(data)
           {

            if(!data.success){
              //alert('data');
               $.each( data.error, function( key, val ) {

                  Materialize.toast('There was an error in the submission', 4000);
                  // alert('data');
                enableFRM();
                });
            } else {
//              if ($('.comments').is(':empty')){
 
                  var structure = [            
                    '<div class="card-panel grey lighten-5 z-depth-1">',
                      '<div class="row valign-wrapper">',
                        '<div class="col s2">',
                          '<img src="http://lorempixel.com/200/200/" alt="" class="circle responsive-img">',
                        '</div>',
                        '<div class="col s10">',
                          '<h5>'+data.details.commentor+'</h5>',
                          '<span class="black-text">',
                            data.details.content,
                          '</span> <br>',
                          '<small class="right"><i>'+data.details.ago+'</i></small>',
                        '</div>',
                      '</div>',
                    '</div>',
                      ];
                      
                      $('.comments > div').append(structure.join(''));
//              }
              Materialize.toast("Comment Added", 4000);
              enableFRM();

            }

           }
         });
    return false; // avoid to execute the actual submit of the form.
});
</script>

@stop